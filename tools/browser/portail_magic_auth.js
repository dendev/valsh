const puppeteer = require('puppeteer');

//check args
if( ! process.env.URL )
    process.exit(1 );
if( ! process.env.USERNAME )
    process.exit(2 );
if( ! process.env.PASSWORD )
    process.exit(3 );

// run
(async () => {
    const browser = await puppeteer.launch({
        headless: false,
        ignoreDefaultArgs: true,
        defaultViewport: null,
        args: [
            //'--incognito', // open double browser :(
            '--maximized', // you can also use '--start-fullscreen'
            '--ignore-certificate-errors',
            '--ignore-certificate-errors-skip-list',
            '--disable-notifications',
            '--disable-background-timer-throttling',
            '--disable-backgrounding-occluded-windows',
            '--disable-breakpad',
            '--disable-component-extensions-with-background-pages',
            '--disable-extensions',
            '--disable-features=TranslateUI,BlinkGenPropertyTrees',
            '--disable-ipc-flooding-protection',
            '--disable-renderer-backgrounding',
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-infobars',
            '--single-process',
            '--no-zygote',
            '--no-first-run',
            '--enable-automation'
        ]
    });

    // callback on close
    browser.on('disconnected', () => {
        console.log( 'Browser closed');
        process.exit(3 );
    });

    // args
    const url = process.env.URL + '/login?ignore';
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    // debug
    console.log( url );

    // page
    // const context = await browser.createIncognitoBrowserContext();
    const page = await browser.newPage();
    await page.goto(url);

    // fill login form
    await page.waitForSelector('body > div > div.login-box-body > form');
    await page.type('body > div > div.login-box-body > form > div:nth-child(2) > input', username);
    await page.type('body > div > div.login-box-body > form > div:nth-child(3) > input', password);

    // submit
    await page.click('body > div > div.login-box-body > form > div.row > div.col-xs-4 > button');
    await page.waitForNavigation();

    //await browser.close(); // keep open
})();

