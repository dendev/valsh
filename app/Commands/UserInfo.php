<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class UserInfo extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "user:info
                            {env=all : Environement où récupérer l'information ( all | proeco || sheldon || portail ) }
                            {username? : login de l'utilisateur }
                            {name? : login de l'utilisateur }
                            {first_name? : login de l'utilisateur }
                            ";

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Infos sur un user';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('--Parse args--');
        $args = $this->_parse_args($this->arguments());

        $this->info('--Args to values--');
        $values = $this->_args_to_values($args);

        $this->info('**Get Info**');
        $infos = $this->_do_action($values);


        $this->info("**OK**");
        if( $infos )
        {
            foreach ($infos as $env => $info )
            {
                $this->info('');
                $this->info(strtoupper($env));

                foreach ($info as $key => $value)
                    $this->info("$key : $value");

                $this->info('');
            }
        }
        else
            $this->error('!! KO !!');

        return $info;
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        // $schedule->command(static::class)->everyMinute();
    }

    private function _parse_args($args)
    {
        $args['from_name'] = false;
        $args['from_username'] = false;

        //  username name first_name
        if(  $args['username'] )
            $args['from_username'] = true;
        else
            if (  $args['name'] || $args['first_name'] )
                $args['from_name'] = true;
            else
                throw new \Exception("!! Un nom d'utilisateur doit être fournit ou un nom prénom !!");

        // env
        if( ! is_array($args['env'] ))
            $args['env'] = [$args['env']];

        // end
        return $args;
    }

    private function _args_to_values($args)
    {
        unset($args['command']);
        return $args;
    }

    private function _do_action($values)
    {
        $refs = [
            'proeco' => 'numproeco',
            'sheldon' => 'people_id',
            'portail' => 'email',
            'inscription' => 'email',
        ];

        $user_identities = $this->_get_user_identities($values);

        $infos = [];
        foreach( $values['env'] as $env )
        {
            $mth = "_get_{$env}_info";

            $key = $refs[$env];
            $user_id = $user_identities->$key;
            $infos[$env] = $this->$mth($user_id);
        }

        return $infos;
    }

    private function _get_user_identities($values)
    {
        $identities = false;

        $username = str_replace('@henallux.be', '', $values['username']);
        if( $values['from_username'] )
        {
            $identities = \DB::connection('sheldon')
                ->table('people')
                ->where('identifiant', $username)
                ->selectRaw("nom AS name, prenom as first_name, identifiant , matricule AS numproeco, id_people, CONCAT(identifiant, '@henallux.be') AS email" )
                ->first();
        }
        else
        {

        }

        if( !$identities )
            throw new \Exception("!! Aucune données trouvés pour $username !!");

        return $identities;
    }

    private function _get_proeco_info($user_id)
    {
    }

    private function _get_portail_info($user_id)
    {
        $info = \DB::connection('portail')
            ->table('users')
            ->where('email', $user_id)
            ->first();

        return ( array ) $info;
    }

    private function _get_sheldon_info($user_id)
    {
    }

    private function _get_inscription_info($user_id)
    {

    }
}
