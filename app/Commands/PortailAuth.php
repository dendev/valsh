<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class PortailAuth extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "portail:auth
                            {env=local : Environement d'execution de l'action ( local || staging || prod ) }
                            {username=dendev : login de l'utilisateur }
                            {mode=normal : mode d'authentification ( normal || magic ) }";

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Connection sur le portail';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('--Args to values--');
        $values = $this->_args_to_values($this->arguments());

        $this->info('--Make cmd--');
        $cmd = $this->_make_tool_cmd($values);

        $this->info('**Go to portail**');
        $ok = $this->_run_cmd($cmd);

        if( $ok )
            $this->info("++It's OK++");
        else
            $this->error('!! KO !!');

        return $ok;
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        // $schedule->command(static::class)->everyMinute();
    }

    private function _args_to_values($args)
    {
        $refs = [
            'env' => [
                'local' => 'http://portail.local',
                'staging' => 'https://portail-staging.henallux.be',
                'prod' => 'https://portail.henallux.be',
            ],
            'username' => [
                'dendev' => 'mdpdevde@henallux.be'
            ],
            'mode' => [
                'normal' => 'normal',
                'magic' => 'magic',
            ],
            'password' => [
                'normal' => env('PORTAIL_USER_PASSWORD'),
                'magic' => env('PORTAIL_MAGIC_PASSWORD')
            ]
        ];
        unset($args['command']);

        $mode = false;
        $values = [];
        foreach( $args as $key => $arg  )
        {
            $values[$key] = false;

            // values from refs
            if( array_key_exists($key, $refs) )
            {
                // by refs
                if (array_key_exists($arg, $refs[$key]))
                {
                    $values[$key] = $refs[$key][$arg];
                }
                else
                {
                    $values[$key] = $this->_set_username($key, $arg);
                }

                // specs
                $values['password'] = $this->_set_password($key, $refs['password'], $arg);
            }
            else
            {
                $this->error("[ArgsToValues] l'argument '$key' n'existe pas dans les refs");
            }
        }

        // debug
        //dd( $values);

        return $values;
    }

    private function _make_tool_cmd($values)
    {
        // cmd
        if( $values['mode'] === 'normal' )
            $cmd = "node ./tools/browser/portail_normal_auth.js";
        else
            $cmd = "node ./tools/browser/portail_magic_auth.js";

        // env vars name
        $refs = [
            'env' => 'URL',
            'username' => 'USERNAME',
            'password' => 'PASSWORD'
        ];

        // env vars
        $env_vars = '';
        foreach( $values as $key => $value )
        {
                if( array_key_exists($key, $refs))
                {
                    $env_var = $refs[$key] . "='" . $value . "' ";
                    $env_vars .= $env_var;
                }
        }

        // cmd
        $cmd_with_env_vars = $env_vars . $cmd;

        // debug
        // dd($cmd_with_env_vars);

        // end
        return $cmd_with_env_vars;
    }

    private function _run_cmd($cmd)
    {
        $ok = shell_exec($cmd);
        $ok = ( is_null($ok) ) ? false: true;

        return $ok;
    }

    private function _set_username($key, $arg)
    {
        $value = false;

        // user mdp or etu
        if( $key === 'username' && ( strstr( $arg, 'etu' ) || ( strstr( $arg, 'mdp' ) ) ) )
            // complete with email ( if needed )
            if( ! strstr($arg, '@henallux.be') )
                $value = $arg . '@henallux.be';
            else
                $value = $arg;
        else
            $this->error("[ArgsToValues] l'argument '$arg' n'existe pas dans les refs");

        return $value;
    }

    private function _set_password($key, $passwords, $arg)
    {
        $value = false;

        // specs
        if( $key === 'mode' )
        {
            $value = $passwords[$arg];
        }

        return $value;
    }
}
