```
         __     __
        /'(  _  )`\
       / . \/^\/ . \
      /  _)_`-'_(_  \
     /.-~   ).(   ~-.\
    /'     /\_/\     `\
           "-V-"
```

https://fr.wikipedia.org/wiki/Valshamr

# Installation

```bash
git clone https://gitlab.com/dendev/valsh.git 
composer install 
npm install 
```

# Configuration

```bash
cp .env.example .env
vim .env
```

Pour utiliser chrome, vérifier sa version et mettre dusk à son niveau
```bash
./application dusk:chrome-driver 85
```


## Portail

### Auth

> env mdp@gmail magic|normal

authentification sur le portail en bypassant le mot de passe 
```
./application portail:auth prod mdpmaph@henallux.be magic
```
